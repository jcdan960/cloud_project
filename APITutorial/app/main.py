#run with :
#uvicorn main:app --reload
#runs on http://127.0.0.1:8000 

from fastapi import FastAPI

db = []

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World from Jici"}