# Requirements
- python3
   

# Installation and Setting up on Linux
```sudo apt-get install python3-pip```

```sudo apt install python3-virtualenv```

```sudo apt install libpq-dev```



```python3 -m venv Cloud_project``

``` source CI_is_fun/bin/activate```

```sudo pip3 install -r requirements.txt```
    
    
When you're done you can deactivate en virtual environnement:

```deactivate```

To freeze the requirements:
```pip freeze -l > requirements.txt``` 

You might need postgresql:
```sudo apt-get install postgresql```
```sudo service postgresql start``` 
```sudo passwd postgres``` 
```su postgres``` 
```psql```
```CREATE DATABASE PRODUCTION;```
```CREATE role dbmanager WITH LOGIN PASSWORD 'superpass';```
```GRANT ALL PRIVILEGES ON DATABASE Production TO dbmanager;```

Other PostGreSQL stuff:
to lists db 
```\list``` 

to connect
```\connect production```

to show tables in db:
```\dt```


to Create table example:
CREATE TABLE COMPANY(
   ID INT PRIMARY KEY     NOT NULL,
   NAME           TEXT    NOT NULL,
   AGE            INT     NOT NULL,
   ADDRESS        CHAR(50),
   SALARY         REAL
);

to insert:
insert into company (id, name, age, address, salary)
values (0,'JC',30,'1302 du millet', 85000)
;


POUR QUE USER PUISSE CREATE ET EDIT DB:
ALTER ROLE db_manager CREATEDB;


```GRANT ALL PRIVILEGES ON ALL TABLEs IN SCHEMA public TO dbmanager;```

To show what is in company:
```select * from company```

```sudo ssh -i JiciTest.pem ubuntu@ec2-3-129-18-71.us-east-2.compute.amazonaws.com```


To deploy the docker container:
```sudo docker build -t myimage .```

```sudo docker run -d --name mycontainer -p 80:80 myimage```

should run on port 80 with :
http://127.0.0.1/
