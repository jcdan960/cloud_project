from DBManager import connect, psycopg2

def run_main():
    try:
        conn = connect("production",  "dbmanager", "superpass")
        cur = conn.cursor()

        salary = 200000
        id = 0

        sql = """ UPDATE company
                SET salary = %s
                WHERE id = %s"""
        cur.execute(sql, (salary, id))

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    conn.commit()
    cur.close()
    conn.close()


if __name__=="__main__":
    run_main()