import sys
import logging
import os 

log_path = "/home/jc/Documents/log"
file_name = "DBManager"

logging.getLogger().addHandler(logging.StreamHandler())

logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

log_formatter = logging.Formatter("%(asctime)s [%(levelname)-5.12s] %(filename)s:%(lineno)s -%(module)s.%(funcName)s:  %(message)s")
root_logger = logging.getLogger()

file_handler = logging.FileHandler("{0}/{1}.log".format(log_path, file_name))
file_handler.setFormatter(log_formatter)
root_logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
root_logger.addHandler(console_handler)
root_logger.setLevel(logging.DEBUG)
