import psycopg2
from logger import root_logger

def connect(dbname, username ,dbpassword):
    root_logger.info("Connecting to DB")
    conn = psycopg2.connect(
        host="localhost",
        database=dbname,
        user=username,
        password=dbpassword)
    
    return conn
